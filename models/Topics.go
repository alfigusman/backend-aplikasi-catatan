package models

import (
	"context"
	"gitlab.com/alfigusman/backend-aplikasi-catatan/config"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"log"
	"time"
)

type Topics struct {
	ID			primitive.ObjectID	`bson:"_id,omitempty" json:"id"`
	Title		string            	`bson:"title,omitempty" json:"title"`
	Tags		[]string            `bson:"tags,omitempty" json:"tags"`
	User    	primitive.ObjectID	`bson:"user,omitempty" json:"user"`
	CreatedAt	time.Time 			`bson:"created_at,omitempty" json:"created_at"`
	UpdatedAt	time.Time			`bson:"updated_at,omitempty" json:"updated_at"`
}

func CreateTopic(topic Topics) interface{} {
	db := config.GetClient()
	collection := db.Database("catatan-dev").Collection("topics")
	insertResult, err := collection.InsertOne(context.TODO(), topic)
	if err != nil {
		log.Fatalln("Error on inserting new Topic", err)
	}
	return insertResult.InsertedID
}

func ReadTopics() []bson.M {
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	db := config.GetClient()
	collection := db.Database("catatan-dev").Collection("topics")
	lookupStage := bson.D{{"$lookup", bson.D{{"from", "users"}, {"localField", "user"}, {"foreignField", "_id"}, {"as", "users"}}}}
	unwindStage := bson.D{{"$unwind", bson.D{{"path", "$user"}, {"preserveNullAndEmptyArrays", false}}}}
	resultCursor, err := collection.Aggregate(ctx, mongo.Pipeline{lookupStage, unwindStage})
	if err != nil {
		panic(err)
	}
	var result []bson.M
	if err = resultCursor.All(ctx, &result); err != nil {
		panic(err)
	}
	return result
}

func UpdateTopic(updatedData bson.M, filter bson.M) int64 {
	db := config.GetClient()
	collection := db.Database("catatan-dev").Collection("topics")
	atualizacao := bson.D{{Key: "$set", Value: updatedData}}
	updatedResult, err := collection.UpdateOne(context.TODO(), filter, atualizacao)
	if err != nil {
		log.Fatal("Error on updating one Topic", err)
	}
	return updatedResult.ModifiedCount
}

func DeleteTopic(filter bson.M) int64 {
	db := config.GetClient()
	collection := db.Database("catatan-dev").Collection("topics")
	deleteResult, err := collection.DeleteOne(context.TODO(), filter)
	if err != nil {
		log.Fatal("Error on deleting one Topic", err)
	}
	return deleteResult.DeletedCount
}

func WhereTopics(match bson.M) []bson.M {
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	db := config.GetClient()
	collection := db.Database("catatan-dev").Collection("topics")
	matchStage := bson.D{{"$match", match}}
	lookupStage := bson.D{{"$lookup", bson.D{{"from", "users"}, {"localField", "user"}, {"foreignField", "_id"}, {"as", "users"}}}}
	unwindStage := bson.D{{"$unwind", bson.D{{"path", "$user"}, {"preserveNullAndEmptyArrays", false}}}}
	resultCursor, err := collection.Aggregate(ctx, mongo.Pipeline{matchStage,lookupStage, unwindStage})
	if err != nil {
		panic(err)
	}
	var result []bson.M
	if err = resultCursor.All(ctx, &result); err != nil {
		panic(err)
	}
	return result
}


