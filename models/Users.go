package models

import (
	"context"
	"gitlab.com/alfigusman/backend-aplikasi-catatan/config"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"log"
	"time"
)

type Users struct {
	ID			primitive.ObjectID	`bson:"_id,omitempty" json:"id"`
	Name		string				`bson:"name,omitempty" json:"name"`
	Email		string				`bson:"email,omitempty" json:"email"`
	Password	string				`bson:"password,omitempty" json:"password"`
	CreatedAt	time.Time			`bson:"created_at,omitempty" json:"created_at"`
	UpdatedAt	time.Time			`bson:"updated_at,omitempty" json:"updated_at"`
}

func CreateUser(user Users) interface{} {
	db := config.GetClient()
	collection := db.Database("catatan-dev").Collection("users")
	insertResult, err := collection.InsertOne(context.TODO(), user)
	if err != nil {
		log.Fatalln("Error on inserting new User", err)
	}
	return insertResult.InsertedID
}

func ReadUsers(filter bson.M) []*Users {
	var users []*Users
	db := config.GetClient()
	collection := db.Database("catatan-dev").Collection("users")
	cur, err := collection.Find(context.TODO(), filter)
	if err != nil {
		log.Fatal("Error on Finding all the documents", err)
	}
	for cur.Next(context.TODO()) {
		var user Users
		err = cur.Decode(&user)
		if err != nil {
			log.Fatal("Error on Decoding the document", err)
		}
		users = append(users, &user)
	}
	return users
}

func UpdateUser(updatedData bson.M, filter bson.M) int64 {
	db := config.GetClient()
	collection := db.Database("catatan-dev").Collection("users")
	atualizacao := bson.D{{Key: "$set", Value: updatedData}}
	updatedResult, err := collection.UpdateOne(context.TODO(), filter, atualizacao)
	if err != nil {
		log.Fatal("Error on updating one User", err)
	}
	return updatedResult.ModifiedCount
}

func DeleteUser(filter bson.M) int64 {
	db := config.GetClient()
	collection := db.Database("catatan-dev").Collection("users")
	deleteResult, err := collection.DeleteOne(context.TODO(), filter)
	if err != nil {
		log.Fatal("Error on deleting one User", err)
	}
	return deleteResult.DeletedCount
}

func WhereUsers(filter bson.M) Users {
	var user Users
	db := config.GetClient()
	collection := db.Database("catatan-dev").Collection("users")
	documentReturned := collection.FindOne(context.TODO(), filter)
	documentReturned.Decode(&user)
	return user
}
