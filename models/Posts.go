package models

import (
	"context"
	"gitlab.com/alfigusman/backend-aplikasi-catatan/config"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"log"
	"time"
)

type Posts struct {
	ID			primitive.ObjectID	`bson:"_id,omitempty" json:"id"`
	Title 		string				`bson:"title,omitempty" json:"title"`
	Post		string            	`bson:"post,omitempty" json:"post"`
	Permission	string            	`bson:"permission,omitempty" json:"permission"`
	Topic		primitive.ObjectID	`bson:"topic,omitempty" json:"topic"`
	User    	primitive.ObjectID	`bson:"user,omitempty" json:"user"`
	CreatedAt	time.Time 			`bson:"created_at,omitempty" json:"created_at"`
	UpdatedAt	time.Time			`bson:"updated_at,omitempty" json:"updated_at"`
}

func CreatePost(post Posts) interface{} {
	db := config.GetClient()
	collection := db.Database("catatan-dev").Collection("posts")
	insertResult, err := collection.InsertOne(context.TODO(), post)
	if err != nil {
		log.Fatalln("Error on inserting new Post", err)
	}
	return insertResult.InsertedID
}

func ReadPosts(permission string) []bson.M {
	db := config.GetClient()
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	collection := db.Database("catatan-dev").Collection("posts")
	if permission == "public" {
		matchStage := bson.D{{"$match", bson.M{"permission": "public"}}}
		lookupStage1 := bson.D{{"$lookup", bson.D{{"from", "topics"}, {"localField", "topic"}, {"foreignField", "_id"}, {"as", "topics"}}}}
		unwindStage1 := bson.D{{"$unwind", bson.D{{"path", "$topic"}, {"preserveNullAndEmptyArrays", false}}}}
		lookupStage2 := bson.D{{"$lookup", bson.D{{"from", "users"}, {"localField", "user"}, {"foreignField", "_id"}, {"as", "users"}}}}
		unwindStage2 := bson.D{{"$unwind", bson.D{{"path", "$user"}, {"preserveNullAndEmptyArrays", false}}}}
		resultCursor, err := collection.Aggregate(ctx, mongo.Pipeline{matchStage ,lookupStage1, unwindStage1, lookupStage2, unwindStage2})
		if err != nil {
			panic(err)
		}
		var result []bson.M
		if err = resultCursor.All(ctx, &result); err != nil {
			panic(err)
		}
		return result
	}else if permission == "private" {
		matchStage := bson.D{{"$match", bson.M{"permission": "private"}}}
		lookupStage1 := bson.D{{"$lookup", bson.D{{"from", "topics"}, {"localField", "topic"}, {"foreignField", "_id"}, {"as", "topics"}}}}
		unwindStage1 := bson.D{{"$unwind", bson.D{{"path", "$topic"}, {"preserveNullAndEmptyArrays", false}}}}
		lookupStage2 := bson.D{{"$lookup", bson.D{{"from", "users"}, {"localField", "user"}, {"foreignField", "_id"}, {"as", "users"}}}}
		unwindStage2 := bson.D{{"$unwind", bson.D{{"path", "$user"}, {"preserveNullAndEmptyArrays", false}}}}
		resultCursor, err := collection.Aggregate(ctx, mongo.Pipeline{matchStage ,lookupStage1, unwindStage1, lookupStage2, unwindStage2})
		if err != nil {
			panic(err)
		}
		var result []bson.M
		if err = resultCursor.All(ctx, &result); err != nil {
			panic(err)
		}
		return result
	}else{
		lookupStage1 := bson.D{{"$lookup", bson.D{{"from", "topics"}, {"localField", "topic"}, {"foreignField", "_id"}, {"as", "topics"}}}}
		unwindStage1 := bson.D{{"$unwind", bson.D{{"path", "$topic"}, {"preserveNullAndEmptyArrays", false}}}}
		lookupStage2 := bson.D{{"$lookup", bson.D{{"from", "users"}, {"localField", "user"}, {"foreignField", "_id"}, {"as", "users"}}}}
		unwindStage2 := bson.D{{"$unwind", bson.D{{"path", "$user"}, {"preserveNullAndEmptyArrays", false}}}}
		resultCursor, err := collection.Aggregate(ctx, mongo.Pipeline{lookupStage1, unwindStage1, lookupStage2, unwindStage2})
		if err != nil {
			panic(err)
		}
		var result []bson.M
		if err = resultCursor.All(ctx, &result); err != nil {
			panic(err)
		}
		return result
	}
}

func UpdatePost(updatedData bson.M, filter bson.M) int64 {
	db := config.GetClient()
	collection := db.Database("catatan-dev").Collection("posts")
	atualizacao := bson.D{{Key: "$set", Value: updatedData}}
	updatedResult, err := collection.UpdateOne(context.TODO(), filter, atualizacao)
	if err != nil {
		log.Fatal("Error on updating one Post", err)
	}
	return updatedResult.ModifiedCount
}

func DeletePost(filter bson.M) int64 {
	db := config.GetClient()
	collection := db.Database("catatan-dev").Collection("posts")
	deleteResult, err := collection.DeleteOne(context.TODO(), filter)
	if err != nil {
		log.Fatal("Error on deleting one Post", err)
	}
	return deleteResult.DeletedCount
}

func WherePosts(where bson.M) []bson.M {
	db := config.GetClient()
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	collection := db.Database("catatan-dev").Collection("posts")
	matchStage := bson.D{{"$match", where}}
	lookupStage1 := bson.D{{"$lookup", bson.D{{"from", "topics"}, {"localField", "topic"}, {"foreignField", "_id"}, {"as", "topics"}}}}
	unwindStage1 := bson.D{{"$unwind", bson.D{{"path", "$topic"}, {"preserveNullAndEmptyArrays", false}}}}
	lookupStage2 := bson.D{{"$lookup", bson.D{{"from", "users"}, {"localField", "user"}, {"foreignField", "_id"}, {"as", "users"}}}}
	unwindStage2 := bson.D{{"$unwind", bson.D{{"path", "$user"}, {"preserveNullAndEmptyArrays", false}}}}
	resultCursor, err := collection.Aggregate(ctx, mongo.Pipeline{matchStage, lookupStage1, unwindStage1, lookupStage2, unwindStage2})
	if err != nil {
		panic(err)
	}
	var result []bson.M
	if err = resultCursor.All(ctx, &result); err != nil {
		panic(err)
	}
	return result
}