package main

import (
	"context"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"gitlab.com/alfigusman/backend-aplikasi-catatan/config"
	"gitlab.com/alfigusman/backend-aplikasi-catatan/controllers"
	"go.mongodb.org/mongo-driver/mongo/readpref"
	"log"
)

func main() {
	/* Check Connection */
	db := config.GetClient()
	err := db.Ping(context.Background(), readpref.Primary())
	if err != nil {
		log.Fatal("Couldn't connect to the database", err)
	} else {
		log.Print("Connected!")
	}

	// Router
	gin.SetMode(gin.ReleaseMode)
	r := gin.Default()
	r.Use(cors.Default())

	r.GET("/test", config.Auth, func(c *gin.Context) {
		c.JSON(200, gin.H{
			"status":   true,
			"message": err,
		})
	})

	// Router Controllers Topics
	r.POST("/Topics", controllers.CreateTopic)
	r.GET("/Topics/", controllers.ReadTopics)
	r.PUT("/Topics/:id", controllers.UpdateTopic)
	r.DELETE("/Topics/:id", controllers.DeleteTopic)
	r.GET("/Topics/:id", controllers.WhereTopics)
	r.GET("/Topics/:id/user", controllers.TopicsFindByUser)
	r.POST("/Topics/title/", controllers.TopicsFindByTitle)

	// Router Controllers Posts
	r.POST("/Posts", controllers.CreatePost)
	r.GET("/Posts/:id/permission", controllers.ReadPosts) //public or etc
	r.PUT("/Posts/:id", controllers.UpdatePost)
	r.DELETE("Posts/:id", controllers.DeletePost)
	r.GET("/Posts/:id", controllers.WherePosts)
	r.GET("/Posts/:id/user", controllers.WherePostsUser)
	r.POST("/Posts/title", controllers.WherePostsTitle)
	r.GET("/Posts/:id/topic/public", controllers.WherePostsTopicPublic)
	r.GET("/Posts/:id/topic/private", controllers.WherePostsPrivate)

	// Router Controllers Users
	r.POST("/Users", controllers.CreateUser)
	r.GET("/Users", controllers.ReadUsers)
	r.PUT("/Users", controllers.UpdateUser)
	r.DELETE("Users/", controllers.DeleteUser)
	r.GET("/Users/:id", controllers.WhereUsers)
	r.POST("/Login", controllers.LoginUsers)
	r.POST("/ChangePassword", controllers.ChangePasswordUsers)


	err = r.Run()
	if err != nil {
		panic(err)
	}
}
