package controllers

import (
	"fmt"
	jwt "github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"github.com/go-ozzo/ozzo-validation/v4"
	"github.com/go-ozzo/ozzo-validation/v4/is"
	"gitlab.com/alfigusman/backend-aplikasi-catatan/config"
	"gitlab.com/alfigusman/backend-aplikasi-catatan/models"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"html"
	"log"
	"strings"
	"time"
)

func validUsers(action int, c *gin.Context) error {
	switch action {
	case 1: //Create
		name := html.EscapeString(strings.TrimSpace(c.PostForm("name")))
		email := html.EscapeString(strings.TrimSpace(c.PostForm("email")))
		password := html.EscapeString(strings.TrimSpace(c.PostForm("password")))
		confirmPassword := html.EscapeString(strings.TrimSpace(c.PostForm("confirm_password")))
		err := validation.Errors{
			"name": validation.Validate(name, validation.Required, validation.Length(2, 50)),
			"email": validation.Validate(email, validation.Required, is.Email),
			"password": validation.Validate(password, validation.Required),
			"confirmPassword": validation.Validate(confirmPassword, validation.Required),
		}.Filter()
		return err
		break
	case 2: //Update
		name := html.EscapeString(strings.TrimSpace(c.PostForm("name")))
		email := html.EscapeString(strings.TrimSpace(c.PostForm("email")))
		err := validation.Errors{
			"name": validation.Validate(name, validation.Required, validation.Length(2, 50)),
			"email": validation.Validate(email, validation.Required, is.Email),
		}.Filter()
		return err
		break
	case 3: //Delete
		id, _ := primitive.ObjectIDFromHex(c.PostForm("id"))
		err := validation.Errors{
			"id": validation.Validate(id, validation.Required),
		}.Filter()
		return err
		break
	case 4: //Where
		id, _ := primitive.ObjectIDFromHex(c.Param("id"))
		err := validation.Errors{
			"id": validation.Validate(id, validation.Required),
		}.Filter()
		return err
		break
	case 5: //Login
		email := html.EscapeString(strings.TrimSpace(c.PostForm("email")))
		password := html.EscapeString(strings.TrimSpace(c.PostForm("password")))
		err := validation.Errors{
			"email": validation.Validate(email, validation.Required, is.Email),
			"password": validation.Validate(password, validation.Required),
		}.Filter()
		return err
		break
	case 6: //Change password
		passwordLama := html.EscapeString(strings.TrimSpace(c.PostForm("old_password")))
		passwordBaru := html.EscapeString(strings.TrimSpace(c.PostForm("new_password")))
		confirmPassword := html.EscapeString(strings.TrimSpace(c.PostForm("confirm_password")))
		err := validation.Errors{
			"old_password": validation.Validate(passwordLama, validation.Required),
			"new_password": validation.Validate(passwordBaru, validation.Required),
			"confirm_password": validation.Validate(confirmPassword, validation.Required),
		}.Filter()
		return err
		break
	}
	return nil
}

func CreateUser(c *gin.Context) {
	err := validUsers(1, c)
	if err != nil {
		fmt.Println(err)
		c.JSON(200, gin.H{
			"status":   true,
			"message": err,
		})
	}else{
		name := html.EscapeString(strings.TrimSpace(c.PostForm("name")))
		email := html.EscapeString(strings.TrimSpace(c.PostForm("email")))
		password := html.EscapeString(strings.TrimSpace(c.PostForm("password")))
		confirmPassword := html.EscapeString(strings.TrimSpace(c.PostForm("confirm_password")))
		where := bson.M{"email": email}
		user := models.WhereUsers(where)
		if user.Email == email {
			c.JSON(200, gin.H{
				"status":   true,
				"message": "Email sudah terdaftar.",
			})
		}else{
			if password == confirmPassword {
				hash, _ := config.HashPassword(password)
				user := models.Users{
					ID: primitive.NewObjectID(),
					Name: name,
					Email: email,
					Password: hash,
					CreatedAt: time.Now(),
					UpdatedAt: time.Now(),
				}
				insertedID := models.CreateUser(user)
				c.JSON(200, gin.H{
					"status":   false,
					"message": "Data berhasil ditambah.",
					"id": insertedID,
				})
			}else{
				c.JSON(200, gin.H{
					"status":   true,
					"message": "Password tidak sama.",
				})
			}
		}
	}
}

func ReadUsers(c *gin.Context) {
	filter := bson.M{}
	users := models.ReadUsers(filter)
	if users != nil {
		c.JSON(200, gin.H{
			"status":   false,
			"message": nil,
			"data":    users,
		})
	}else{
		c.JSON(200, gin.H{
			"status":   false,
			"message": "Data kosong.",
		})
	}
}

func UpdateUser(c *gin.Context) {
	err := validUsers(2, c)
	if err != nil {
		fmt.Println(err)
		c.JSON(200, gin.H{
			"status":   true,
			"message": err,
		})
	}else{
		id, err := primitive.ObjectIDFromHex(c.PostForm("id"))
		if err != nil {
			log.Print("primitive.ObjectIDFromHex ERROR:", err)
			c.JSON(200, gin.H{
				"status":   true,
				"message": "primitive.ObjectIDFromHex ERROR : cek log for details",
			})
		} else {
			email := html.EscapeString(strings.TrimSpace(c.PostForm("email")))
			name := html.EscapeString(strings.TrimSpace(c.PostForm("name")))
			where := bson.M{"email": email}
			user := models.WhereUsers(where)
			if user.Email == email {
				c.JSON(200, gin.H{
					"status":   true,
					"message": "Email sudah terdaftar.",
				})
			}else{
				update := bson.M{
					"name": name,
					"email": email,
					"updated_at": time.Now(),
				}
				where := bson.M{"_id": id}
				user := models.UpdateUser(update, where)
				if user > 0 {
					c.JSON(200, gin.H{
						"status":   false,
						"message": "User has been updated.",
					})
				} else {
					c.JSON(200, gin.H{
						"status":   true,
						"message": "User is not found.",
					})
				}
			}
		}
	}
}

func DeleteUser(c *gin.Context) {
	err := validUsers(3, c)
	if err != nil {
		fmt.Println(err)
		c.JSON(200, gin.H{
			"status":   true,
			"message": err,
		})
	}else{
		id, err := primitive.ObjectIDFromHex(c.PostForm("id"))
		if err != nil {
			log.Print("primitive.ObjectIDFromHex ERROR:", err)
			c.JSON(200, gin.H{
				"status":   true,
				"message": "primitive.ObjectIDFromHex ERROR : cek log for details",
			})
		} else {
			where := bson.M{"_id": id}
			user := models.DeleteUser(where)
			if user > 0 {
				c.JSON(200, gin.H{
					"status":   false,
					"message": "User has been removed.",
				})
			} else {
				c.JSON(200, gin.H{
					"status":   true,
					"message": "User is not found.",
				})
			}
		}
	}
}

func WhereUsers(c *gin.Context) {
	err := validUsers(4, c)
	if err != nil {
		fmt.Println(err)
		c.JSON(200, gin.H{
			"status":   true,
			"message": err,
		})
	}else{
		id, err := primitive.ObjectIDFromHex(c.Param("id"))
		if err != nil {
			log.Print("primitive.ObjectIDFromHex ERROR:", err)
			c.JSON(200, gin.H{
				"status":   true,
				"message": "primitive.ObjectIDFromHex ERROR : cek log for details",
			})
		} else {
			where := bson.M{"_id": id}
			user := models.WhereUsers(where)
			c.JSON(200, gin.H{
				"status":   false,
				"message": nil,
				"data":    user,
			})
		}
	}
}

func LoginUsers(c *gin.Context) {
	err := validUsers(5, c)
	if err != nil {
		fmt.Println(err)
		c.JSON(200, gin.H{
			"status":   true,
			"message": err,
		})
	}else{
		email := html.EscapeString(strings.TrimSpace(c.PostForm("email")))
		password := html.EscapeString(strings.TrimSpace(c.PostForm("password")))
		where := bson.M{"email": email}
		user := models.WhereUsers(where)
		if user.Email == email {
			match := config.CheckPasswordHash(password, user.Password)
			if match {
				sign := jwt.New(jwt.GetSigningMethod("HS256"))
				token, _ := sign.SignedString([]byte("secret"))
				c.JSON(200, gin.H{
					"status":   false,
					"message": nil,
					"data": user,
					"token": token,
				})
			}else{
				c.JSON(200, gin.H{
					"status":   true,
					"message": "Password salah.",
				})
			}
		}else{
			c.JSON(200, gin.H{
				"status":   true,
				"message": "Email tidak terdaftar.",
			})
		}
	}
}

func ChangePasswordUsers(c *gin.Context) {
	err := validUsers(6, c)
	if err != nil {
		fmt.Println(err)
		c.JSON(200, gin.H{
			"status":   true,
			"message": err,
		})
	}else{
		id, err := primitive.ObjectIDFromHex(c.PostForm("id"))
		if err != nil {
			log.Print("primitive.ObjectIDFromHex ERROR", err)
			c.JSON(200, gin.H{
				"status":   true,
				"message": "primitive.ObjectIDFromHex ERROR : check log for details",
			})
		} else {
			where := bson.M{"_id": id}
			user := models.WhereUsers(where)
			passwordLama := html.EscapeString(strings.TrimSpace(c.PostForm("old_password")))
			passwordBaru := html.EscapeString(strings.TrimSpace(c.PostForm("new_password")))
			confirmPassword := html.EscapeString(strings.TrimSpace(c.PostForm("confirm_password")))
			match := config.CheckPasswordHash(passwordLama, user.Password)
			if match {
				if passwordBaru == confirmPassword {
					hash, _ := config.HashPassword(passwordBaru)
					update := bson.M{
						"password": hash,
						"updated_at": time.Now(),
					}
					where := bson.M{"_id": id}
					user := models.UpdateUser(update, where)
					if user > 0 {
						c.JSON(200, gin.H{
							"status":   false,
							"message": "User password has been updated.",
						})
					} else {
						c.JSON(200, gin.H{
							"status":   true,
							"message": "User is not found.",
						})
					}
				}else{
					c.JSON(200, gin.H{
						"status":   true,
						"message": "Password tidak sama.",
					})
				}
			}else{
				c.JSON(200, gin.H{
					"status":   true,
					"message": "Password salah.",
				})
			}
		}
	}
}

