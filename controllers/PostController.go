package controllers

import (
	"fmt"
	"github.com/gin-gonic/gin"
	validation "github.com/go-ozzo/ozzo-validation/v4"
	"gitlab.com/alfigusman/backend-aplikasi-catatan/models"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"html"
	"log"
	"strings"
	"time"
)

func validPost(action int, c *gin.Context) error {
	switch action {
	case 1: //Create
		title := c.PostForm("title")
		posting := c.PostForm("post")
		permission := html.EscapeString(strings.TrimSpace(c.PostForm("permission")))
		topic, _ := primitive.ObjectIDFromHex(c.PostForm("topic"))
		user, _ := primitive.ObjectIDFromHex(c.PostForm("user"))
		err := validation.Errors{
			"title": validation.Validate(title, validation.Required),
			"post": validation.Validate(posting, validation.Required),
			"permission": validation.Validate(permission, validation.Required),
			"topic": validation.Validate(topic, validation.Required),
			"user": validation.Validate(user, validation.Required),
		}.Filter()
		return err
		break
	case 2: //Update
		id, _ := primitive.ObjectIDFromHex(c.Param("id"))
		title := c.PostForm("title")
		posting := c.PostForm("post")
		permission := html.EscapeString(strings.TrimSpace(c.PostForm("permission")))
		topic, _ := primitive.ObjectIDFromHex(c.PostForm("topic"))
		user, _ := primitive.ObjectIDFromHex(c.PostForm("user"))
		err := validation.Errors{
			"id": validation.Validate(id, validation.Required),
			"title": validation.Validate(title, validation.Required),
			"post": validation.Validate(posting, validation.Required),
			"permission": validation.Validate(permission, validation.Required),
			"topic": validation.Validate(topic, validation.Required),
			"user": validation.Validate(user, validation.Required),
		}.Filter()
		return err
		break
	case 3: //Delete
		id, _ := primitive.ObjectIDFromHex(c.Param("id"))
		err := validation.Errors{
			"id": validation.Validate(id, validation.Required),
		}.Filter()
		return err
		break
	case 4: //Where
		id, _ := primitive.ObjectIDFromHex(c.Param("id"))
		err := validation.Errors{
			"id": validation.Validate(id, validation.Required),
		}.Filter()
		return err
		break
	case 5: //Where
		title := html.EscapeString(strings.TrimSpace(c.PostForm("title")))
		err := validation.Errors{
			"title": validation.Validate(title, validation.Required),
		}.Filter()
		return err
		break
	}
	return nil
}

func CreatePost(c *gin.Context) {
	err := validPost(1, c)
	if err != nil {
		fmt.Println(err)
		c.JSON(200, gin.H{
			"error":   true,
			"message": err,
		})
	}else{
		topic, err := primitive.ObjectIDFromHex(c.PostForm("topic"))
		if err != nil {
			log.Print("ID Topic primitive.ObjectIDFromHex ERROR:", err)
			c.JSON(200, gin.H{
				"error":   true,
				"message": "ID Topic primitive.ObjectIDFromHex ERROR : cek log for details",
			})
		}else{
			user, err := primitive.ObjectIDFromHex(c.PostForm("user"))
			if err != nil {
				log.Print("ID User primitive.ObjectIDFromHex ERROR:", err)
				c.JSON(200, gin.H{
					"error":   true,
					"message": "ID User primitive.ObjectIDFromHex ERROR : cek log for details",
				})
			}else{
				title := c.PostForm("title")
				posting := c.PostForm("post")
				permission := html.EscapeString(strings.TrimSpace(c.PostForm("permission")))
				post := models.Posts{
					ID: primitive.NewObjectID(),
					Title: title,
					Post: posting,
					Permission: permission,
					Topic: topic,
					User: user,
					CreatedAt: time.Now(),
					UpdatedAt: time.Now(),
				}
				insertedID := models.CreatePost(post)
				if insertedID != nil {
					c.JSON(200, gin.H{
						"status":   false,
						"message": "Berhasil menambah Posts.",
						"id": insertedID,
					})
				}else{
					c.JSON(200, gin.H{
						"status":   true,
						"message": "Gagal menambah Posts.",
					})
				}
			}
		}
	}
}

func ReadPosts(c *gin.Context) {
	permission := html.EscapeString(strings.TrimSpace(c.Param("id")))
	posts := models.ReadPosts(permission)
	if posts != nil {
		c.JSON(200, gin.H{
			"status":   false,
			"message": nil,
			"data":    posts,
		})
	}else{
		c.JSON(200, gin.H{
			"status":   true,
			"message": "Data kosong.",
		})
	}
}

func UpdatePost(c *gin.Context) {
	err := validPost(2, c)
	if err != nil {
		fmt.Println(err)
		c.JSON(200, gin.H{
			"error":   true,
			"message": err,
		})
	}else{
		id, err := primitive.ObjectIDFromHex(c.Param("id"))
		if err != nil {
			log.Print("ID primitive.ObjectIDFromHex ERROR:", err)
			c.JSON(200, gin.H{
				"error":   true,
				"message": "ID primitive.ObjectIDFromHex ERROR : cek log for details",
			})
		}else{
			topic, err := primitive.ObjectIDFromHex(c.PostForm("topic"))
			if err != nil {
				log.Print("ID Topic primitive.ObjectIDFromHex ERROR:", err)
				c.JSON(200, gin.H{
					"error":   true,
					"message": "ID Topic primitive.ObjectIDFromHex ERROR : cek log for details",
				})
			}else{
				user, err := primitive.ObjectIDFromHex(c.PostForm("user"))
				if err != nil {
					log.Print("ID User primitive.ObjectIDFromHex ERROR:", err)
					c.JSON(200, gin.H{
						"error":   true,
						"message": "ID User primitive.ObjectIDFromHex ERROR : cek log for details",
					})
				}else{
					title := c.PostForm("title")
					posting := c.PostForm("post")
					permission := html.EscapeString(strings.TrimSpace(c.PostForm("permission")))
					update := bson.M{
						"title": title,
						"post": posting,
						"permission": permission,
						"topic": topic,
						"user": user,
						"updated_at": time.Now(),
					}
					where := bson.M{"_id": id}
					post := models.UpdatePost(update, where)
					if post > 0 {
						c.JSON(200, gin.H{
							"status":   false,
							"message": "Post has been updated.",
						})
					} else {
						c.JSON(200, gin.H{
							"status":   true,
							"message": "Post is not found.",
						})
					}
				}
			}
		}
	}
}

func DeletePost(c *gin.Context) {
	err := validPost(3, c)
	if err != nil {
		fmt.Println(err)
		c.JSON(200, gin.H{
			"error":   true,
			"message": err,
		})
	}else{
		id, err := primitive.ObjectIDFromHex(c.Param("id"))
		if err != nil {
			log.Print("ID primitive.ObjectIDFromHex ERROR:", err)
			c.JSON(200, gin.H{
				"error":   true,
				"message": "ID primitive.ObjectIDFromHex ERROR : cek log for details",
			})
		}else{
			where := bson.M{"_id": id}
			post := models.DeletePost(where)
			if post > 0 {
				c.JSON(200, gin.H{
					"status":   false,
					"message": "Post has been removed.",
				})
			} else {
				c.JSON(200, gin.H{
					"status":   true,
					"message": "Post is not found.",
				})
			}
		}
	}
}

func WherePosts(c *gin.Context) {
	err := validPost(4, c)
	if err != nil {
		fmt.Println(err)
		c.JSON(200, gin.H{
			"error":   true,
			"message": err,
		})
	}else{
		id, err := primitive.ObjectIDFromHex(c.Param("id"))
		if err != nil {
			log.Print("ID primitive.ObjectIDFromHex ERROR:", err)
			c.JSON(200, gin.H{
				"error":   true,
				"message": "ID primitive.ObjectIDFromHex ERROR : cek log for details",
			})
		}else{
			where := bson.M{"_id": id}
			post := models.WherePosts(where)
			if post != nil {
				c.JSON(200, gin.H{
					"status":   false,
					"message": nil,
					"data":    post,
				})
			}else{
				c.JSON(200, gin.H{
					"status":   true,
					"message": "Data kosong.",
				})
			}
		}
	}
}

func WherePostsUser(c *gin.Context) {
	err := validPost(4, c)
	if err != nil {
		fmt.Println(err)
		c.JSON(200, gin.H{
			"error":   true,
			"message": err,
		})
	}else{
		id, err := primitive.ObjectIDFromHex(c.Param("id"))
		if err != nil {
			log.Print("ID User primitive.ObjectIDFromHex ERROR:", err)
			c.JSON(200, gin.H{
				"error":   true,
				"message": "ID User primitive.ObjectIDFromHex ERROR : cek log for details",
			})
		}else{
			where := bson.M{"user": id}
			post := models.WherePosts(where)
			if post != nil {
				c.JSON(200, gin.H{
					"status":   false,
					"message": nil,
					"data":    post,
				})
			}else{
				c.JSON(200, gin.H{
					"status":   true,
					"message": "Data kosong.",
				})
			}
		}
	}
}

func WherePostsTitle(c *gin.Context) {
	err := validPost(4, c)
	if err != nil {
		fmt.Println(err)
		c.JSON(200, gin.H{
			"error":   true,
			"message": err,
		})
	}else{
		title := html.EscapeString(strings.TrimSpace(c.PostForm("title")))

		where := bson.M{"title": title}
		post := models.WherePosts(where)
		if post != nil {
			c.JSON(200, gin.H{
				"status":   false,
				"message": nil,
				"data":    post,
			})
		}else{
			c.JSON(200, gin.H{
				"status":   true,
				"message": "Data kosong.",
			})
		}

	}
}

func WherePostsTopicPublic(c *gin.Context) {
	err := validPost(4, c)
	if err != nil {
		fmt.Println(err)
		c.JSON(200, gin.H{
			"error":   true,
			"message": err,
		})
	}else{
		id, err := primitive.ObjectIDFromHex(c.Param("id"))
		if err != nil {
			log.Print("ID primitive.ObjectIDFromHex ERROR:", err)
			c.JSON(200, gin.H{
				"error":   true,
				"message": "ID primitive.ObjectIDFromHex ERROR : cek log for details",
			})
		}else{
			where := bson.M{"topic": id, "permission": "public"}
			post := models.WherePosts(where)
			if post != nil {
				c.JSON(200, gin.H{
					"status":   false,
					"message": nil,
					"data":    post,
				})
			}else{
				c.JSON(200, gin.H{
					"status":   true,
					"message": "Data kosong.",
				})
			}
		}
	}
}

func WherePostsPrivate(c *gin.Context) {
	err := validPost(4, c)
	if err != nil {
		fmt.Println(err)
		c.JSON(200, gin.H{
			"error":   true,
			"message": err,
		})
	}else{
		id, err := primitive.ObjectIDFromHex(c.Param("id"))
		if err != nil {
			log.Print("ID User primitive.ObjectIDFromHex ERROR:", err)
			c.JSON(200, gin.H{
				"error":   true,
				"message": "ID User primitive.ObjectIDFromHex ERROR : cek log for details",
			})
		}else{
			where := bson.M{"user": id, "permission": "private"}
			post := models.WherePosts(where)
			if post != nil {
				c.JSON(200, gin.H{
					"status":   false,
					"message": nil,
					"data":    post,
				})
			}else{
				c.JSON(200, gin.H{
					"status":   true,
					"message": "Data kosong.",
				})
			}
		}
	}
}
