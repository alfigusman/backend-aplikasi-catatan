package controllers

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/go-ozzo/ozzo-validation/v4"
	"gitlab.com/alfigusman/backend-aplikasi-catatan/models"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"html"
	"log"
	"strings"
	"time"
)

func validTopic(action int, c *gin.Context) error {
	switch action {
	case 1: //Create
		user, _ := primitive.ObjectIDFromHex(c.PostForm("user"))
		title := html.EscapeString(strings.TrimSpace(c.PostForm("title")))
		tags := html.EscapeString(strings.TrimSpace(c.PostForm("tags")))
		err := validation.Errors{
			"user": validation.Validate(user, validation.Required),
			"title": validation.Validate(title, validation.Required),
			"tags": validation.Validate(tags, validation.Required),
		}.Filter()
		return err
		break
	case 2: //Update
		id, _ := primitive.ObjectIDFromHex(c.Param("id"))
		user, _ := primitive.ObjectIDFromHex(c.PostForm("user"))
		title := html.EscapeString(strings.TrimSpace(c.PostForm("title")))
		tags := html.EscapeString(strings.TrimSpace(c.PostForm("tags")))
		err := validation.Errors{
			"id": validation.Validate(id, validation.Required),
			"user": validation.Validate(user, validation.Required),
			"title": validation.Validate(title, validation.Required),
			"tags": validation.Validate(tags, validation.Required),
		}.Filter()
		return err
		break
	case 3: //Delete
		id, _ := primitive.ObjectIDFromHex(c.Param("id"))
		err := validation.Errors{
			"id": validation.Validate(id, validation.Required),
		}.Filter()
		return err
		break
	case 4: //Where
		id, _ := primitive.ObjectIDFromHex(c.Param("id"))
		err := validation.Errors{
			"id": validation.Validate(id, validation.Required),
		}.Filter()
		return err
		break
	case 5: //FindByTitle
		title := html.EscapeString(strings.TrimSpace(c.PostForm("title")))
		err := validation.Errors{
			"title": validation.Validate(title, validation.Required),
		}.Filter()
		return err
		break
	}
	return nil
}

func CreateTopic(c *gin.Context) {
	err := validTopic(1, c)
	if err != nil {
		fmt.Println(err)
		c.JSON(200, gin.H{
			"error":   true,
			"message": err,
		})
	}else{
		user, err := primitive.ObjectIDFromHex(c.PostForm("user"))
		if err != nil {
			log.Print("ID User primitive.ObjectIDFromHex ERROR:", err)
			c.JSON(200, gin.H{
				"error":   true,
				"message": "ID User primitive.ObjectIDFromHex ERROR : cek log for details",
			})
		}else{
			title := html.EscapeString(strings.TrimSpace(c.PostForm("title")))
			tags := html.EscapeString(strings.TrimSpace(c.PostForm("tags")))
			tagsSplit := strings.Split(tags, ",")
			topic := models.Topics{
				ID: primitive.NewObjectID(),
				Title: title,
				Tags: tagsSplit,
				User: user,
				CreatedAt: time.Now(),
				UpdatedAt: time.Now(),
			}
			insertedID := models.CreateTopic(topic)
			if insertedID != nil {
				c.JSON(200, gin.H{
					"status":   false,
					"message": "Topics berhasil ditambahkan.",
					"id": insertedID,
				})
			}else{
				c.JSON(200, gin.H{
					"status":   true,
					"message": "Gagal menambah Topics.",
				})
			}
		}
	}
}

func ReadTopics(c *gin.Context) {
	topics := models.ReadTopics()
	if topics != nil {
		c.JSON(200, gin.H{
			"status":   false,
			"message": nil,
			"data":    topics,
		})
	}else{
		c.JSON(200, gin.H{
			"status":   true,
			"message": "Data kosong.",
		})
	}
}

func UpdateTopic(c *gin.Context) {
	err := validTopic(2, c)
	if err != nil {
		fmt.Println(err)
		c.JSON(200, gin.H{
			"error":   true,
			"message": err,
		})
	}else{
		id, err := primitive.ObjectIDFromHex(c.Param("id"))
		if err != nil {
			log.Print("ID primitive.ObjectIDFromHex ERROR:", err)
			c.JSON(200, gin.H{
				"error":   true,
				"message": "ID primitive.ObjectIDFromHex ERROR : cek log for details",
			})
		}else{
			user, err := primitive.ObjectIDFromHex(c.PostForm("user"))
			if err != nil {
				log.Print("ID User primitive.ObjectIDFromHex ERROR:", err)
				c.JSON(200, gin.H{
					"error":   true,
					"message": "ID User primitive.ObjectIDFromHex ERROR : cek log for details",
				})
			}else{
				title := html.EscapeString(strings.TrimSpace(c.PostForm("title")))
				tags := html.EscapeString(strings.TrimSpace(c.PostForm("tags")))
				tagsSplit := strings.Split(tags, ",")
				update := bson.M{
					"title": title,
					"tags":  tagsSplit,
					"user":  user,
					"updated_at": time.Now(),
				}
				where := bson.M{"_id": id}
				topic := models.UpdateTopic(update, where)
				if topic > 0 {
					c.JSON(200, gin.H{
						"status":   false,
						"message": "Topic has been updated.",
					})
				} else {
					c.JSON(200, gin.H{
						"status":   true,
						"message": "Topic is not found.",
					})
				}
			}
		}
	}
}

func DeleteTopic(c *gin.Context) {
	err := validTopic(3, c)
	if err != nil {
		fmt.Println(err)
		c.JSON(200, gin.H{
			"error":   true,
			"message": err,
		})
	}else{
		id, err := primitive.ObjectIDFromHex(c.Param("id"))
		if err != nil {
			log.Print("ID primitive.ObjectIDFromHex ERROR:", err)
			c.JSON(200, gin.H{
				"error":   true,
				"message": "ID primitive.ObjectIDFromHex ERROR : cek log for details",
			})
		}else{
			where := bson.M{"_id": id}
			topic := models.DeleteTopic(where)
			if topic > 0 {
				c.JSON(200, gin.H{
					"status":   false,
					"message": "Topic has been removed.",
				})
			} else {
				c.JSON(200, gin.H{
					"status":   true,
					"message": "Topic is not found.",
				})
			}
		}
	}
}

func WhereTopics(c *gin.Context) {
	err := validTopic(4, c)
	if err != nil {
		fmt.Println(err)
		c.JSON(200, gin.H{
			"error":   true,
			"message": err,
		})
	}else{
		id, err := primitive.ObjectIDFromHex(c.Param("id"))
		if err != nil {
			log.Print("ID primitive.ObjectIDFromHex ERROR:", err)
			c.JSON(200, gin.H{
				"error":   true,
				"message": "ID primitive.ObjectIDFromHex ERROR : cek log for details",
			})
		}else{
			where := bson.M{"_id": id}
			topic := models.WhereTopics(where)
			if topic != nil {
				c.JSON(200, gin.H{
					"status":   false,
					"message": nil,
					"data":    topic,
				})
			}else{
				c.JSON(200, gin.H{
					"status":   true,
					"message": "Data kosong.",
				})
			}
		}
	}
}

func TopicsFindByUser(c *gin.Context) {
	err := validTopic(4, c)
	if err != nil {
		fmt.Println(err)
		c.JSON(200, gin.H{
			"error":   true,
			"message": err,
		})
	}else{
		id, err := primitive.ObjectIDFromHex(c.Param("id"))
		if err != nil {
			log.Print("ID User primitive.ObjectIDFromHex ERROR:", err)
			c.JSON(200, gin.H{
				"error":   true,
				"message": "ID User primitive.ObjectIDFromHex ERROR : cek log for details",
			})
		}else{
			where := bson.M{"user": id}
			topic := models.WhereTopics(where)
			if topic != nil {
				c.JSON(200, gin.H{
					"status":   false,
					"message": nil,
					"data":    topic,
				})
			}else{
				c.JSON(200, gin.H{
					"status":   true,
					"message": "Data kosong.",
				})
			}
		}
	}
}

func TopicsFindByTitle(c *gin.Context) {
	err := validTopic(5, c)
	if err != nil {
		fmt.Println(err)
		c.JSON(200, gin.H{
			"error":   true,
			"message": err,
		})
	}else{
		title := html.EscapeString(strings.TrimSpace(c.PostForm("title")))
		where := bson.M{"title": title}
		topic := models.WhereTopics(where)
		if topic != nil {
			c.JSON(200, gin.H{
				"status":   false,
				"message": nil,
				"data":    topic,
			})
		}else{
			c.JSON(200, gin.H{
				"status":   true,
				"message": "Data kosong.",
			})
		}

	}
}

