package controllers

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/alfigusman0/belajar-golang-dan-mongodb/config"
	"gitlab.com/alfigusman0/belajar-golang-dan-mongodb/models"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"log"
)

func CreateIdea(c *gin.Context) {
	db := config.GetClient()
	idea := models.Ideas{
		ID: primitive.NewObjectID(),
		Title: c.PostForm("title"),
		Details: c.PostForm("details"),
		User: c.PostForm("user"),
	}
	insertedID := models.CreateIdea(db, idea)
	c.JSON(200, gin.H{
		"error":   true,
		"message": insertedID,
	})
	//log.Println(insertedID)
}

func ReadIdeas(c *gin.Context) {
	db := config.GetClient()
	filter := bson.M{}
	ideas := models.ReadIdeas(db, filter)
	c.JSON(200, gin.H{
		"error":   false,
		"message": nil,
		"data":    ideas,
	})
}

func UpdateIdea(c *gin.Context) {
	db := config.GetClient()
	// Declare a primitive ObjectID from a hexadecimal string
	idPrimitive, err := primitive.ObjectIDFromHex(c.PostForm("id"))
	if err != nil {
		log.Fatal("primitive.ObjectIDFromHex ERROR:", err)
	} else {
		update := bson.M{
			"title":   c.PostForm("title"),
			"details": c.PostForm("details"),
			"user":    c.PostForm("user"),
		}
		where := bson.M{"_id": idPrimitive}
		idea := models.UpdateIdea(db, update, where)
		if idea > 0 {
			c.JSON(200, gin.H{
				"error":   false,
				"message": "Idea has been updated.",
			})
		} else {
			c.JSON(200, gin.H{
				"error":   true,
				"message": "Idea is not found.",
			})
		}
	}
}

func DeleteIdea(c *gin.Context) {
	db := config.GetClient()
	// Declare a primitive ObjectID from a hexadecimal string
	idPrimitive, err := primitive.ObjectIDFromHex(c.PostForm("id"))
	if err != nil {
		log.Fatal("primitive.ObjectIDFromHex ERROR:", err)
	} else {
		where := bson.M{"_id": idPrimitive}
		idea := models.DeleteIdea(db, where)
		if idea > 0 {
			c.JSON(200, gin.H{
				"error":   false,
				"message": "Idea has been removed.",
			})
		} else {
			c.JSON(200, gin.H{
				"error":   true,
				"message": "Idea is not found.",
			})
		}
	}
}

func WhereIdeas(c *gin.Context) {
	db := config.GetClient()
	idPrimitive, err := primitive.ObjectIDFromHex(c.Param("id"))
	if err != nil {
		log.Fatal("primitive.ObjectIDFromHex ERROR:", err)
	} else {
		where := bson.M{"_id": idPrimitive}
		idea := models.WhereIdeas(db, where)
		c.JSON(200, gin.H{
			"error":   false,
			"message": nil,
			"data":    idea,
		})
	}
}
